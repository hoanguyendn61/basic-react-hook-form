import logo from './logo.svg';
import './App.css';
import { Form } from './features/forms/Form';
function App() {
  return (
    <>
      <div className="container d-flex flex-column justify-content-center align-items-center">
        <h1>Basic React Hook Form</h1>
        <Form />
      </div>
    </>
  );
}

export default App;
