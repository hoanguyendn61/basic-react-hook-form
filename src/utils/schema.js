import * as yup from "yup";
const schema = yup.object({
  firstName: yup.string().required("First Name is required"),
  lastName: yup.string().required("Last Name is required"),
  email: yup.string().email().required("Email is required"),
  dob: yup
    .date()
    .required("Date of birth is required")
    .max(new Date(), "Date of birth must be in the past")
    .typeError("Date of birth is required"),

  phone: yup
    .string()
    .required("Phone is required")
    .matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/, "Phone number must be valid"),
  password: yup
    .string()
    .required("Password is required")
    .matches(
      /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/,
      "Password must be at least 8 characters, " +
        "contain at least one lowercase letter, " +
        "one uppercase letter, one number and one special character"
    ),
  confirmPassword: yup
    .string()
    .required("Confirm Password is required")
    .oneOf([yup.ref("password"), null], "Passwords must match"),
  emotion: yup
    .array()
    .min(1, "You must choose at least one emotion")
    .of(yup.string())
    .typeError("You must choose at least one emotion"),
});

export default schema;
