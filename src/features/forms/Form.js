import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import schema from "../../utils/schema";

export const Form = (props) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = (data) => {
    console.log(data);
  };
  // console.log(watch("first_name")); // watch input value by passing the name of it
  return (
    /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
    <form class="w-50" onSubmit={handleSubmit(onSubmit)}>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">First Name</label>
          <input
            type="text"
            {...register("firstName")}
            className={`form-control ${
              errors.firstName ? "border-danger" : ""
            }`}
            placeholder="First name"
            aria-label="First name"
          />
          {errors.firstName && (
            <span className="text-danger">{errors.firstName.message}</span>
          )}
        </div>
        <div className="col">
          <label className="form-label">Last Name</label>
          <input
            type="text"
            {...register("lastName")}
            className={`form-control ${errors.lastName ? "border-danger" : ""}`}
            placeholder="Last name"
            aria-label="Last name"
          />
          {errors.lastName && (
            <span className="text-danger">{errors.lastName.message}</span>
          )}
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">Email</label>
          <input
            type="text"
            {...register("email")}
            className={`form-control ${errors.lastName ? "border-danger" : ""}`}
            placeholder="Email"
            aria-label="Email"
          />
          {errors.email && (
            <span className="text-danger">{errors.email.message}</span>
          )}
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">Phone</label>
          <input
            type="text"
            {...register("phone")}
            className={`form-control ${errors.phone ? "border-danger" : ""}`}
            placeholder="Phone"
          />
          {errors.phone && (
            <div className="text-danger">{errors.phone.message}</div>
          )}
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">Password</label>
          <input
            type="password"
            {...register("password")}
            className={`form-control ${errors.password ? "border-danger" : ""}`}
            placeholder="Password"
          />
          {errors.password && (
            <div
              className="text-danger text-wrap"
              style={{
                width: "100%",
              }}
            >
              {errors.password.message}
            </div>
          )}
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">Confirm Password</label>
          <input
            type="password"
            {...register("confirmPassword")}
            className={`form-control ${
              errors.confirmPassword ? "border-danger" : ""
            }`}
            placeholder="Confirm Password"
          />
          {errors.confirmPassword && (
            <span className="text-danger">
              {errors.confirmPassword.message}
            </span>
          )}
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">Date of birth</label>
          <input
            type="date"
            {...register("dob")}
            className={`form-control ${errors.dob ? "border-danger" : ""}`}
            placeholder="Date of birth"
            aria-label="dob"
          />
          {errors.dob && (
            <span className="text-danger">{errors.dob.message}</span>
          )}
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">Gender</label>
          <div className="d-flex">
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="gender"
                id="flexRadioDefault1"
                defaultChecked
              />
              <label className="form-check-label" htmlFor="flexRadioDefault1">
                Male
              </label>
            </div>
            <div className="mx-4 form-check">
              <input
                className="form-check-input"
                type="radio"
                name="gender"
                id="flexRadioDefault2"
              />
              <label className="form-check-label" htmlFor="flexRadioDefault2">
                Female
              </label>
            </div>
          </div>
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label className="form-label">How did you hear about us?</label>
          <select
            {...register("aboutUs")}
            className={`form-select ${errors.aboutUs ? "border-danger" : ""}`}
          >
            <option defaultValue="idx1">Through a friend</option>
            <option defaultValue="idx2">Social media</option>
            <option defaultValue="idx3">
              Recommended by friend or colleague
            </option>
            <option defaultValue="idx4">Other...</option>
          </select>

          {errors.aboutUs && (
            <span className="text-danger">{errors.aboutUs.message}</span>
          )}
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <div className="d-flex flex-column">
            <label className="form-label">
              Please check all the emotions that apply to you
            </label>
            {errors.emotion && (
              <span className="text-danger">{errors.emotion.message}</span>
            )}
          </div>
          <div>
            <div className="input-group mb-3">
              <div className="input-group-text">
                <input
                  className="form-check-input mt-0"
                  {...register("emotion")}
                  type="checkbox"
                  defaultValue="Angry"
                  aria-label="Checkbox for following text input"
                />
              </div>
              <input
                type="text"
                defaultValue="Angry"
                disabled
                className="form-control"
                aria-label="Text input with checkbox"
              />
            </div>
            <div className="input-group mb-3">
              <div className="input-group-text">
                <input
                  className="form-check-input mt-0"
                  {...register("emotion")}
                  type="checkbox"
                  defaultValue="Sad"
                  aria-label="Checkbox for following text input"
                />
              </div>
              <input
                type="text"
                defaultValue="Sad"
                disabled
                className="form-control"
                aria-label="Text input with checkbox"
              />
            </div>
            <div className="input-group mb-3">
              <div className="input-group-text">
                <input
                  className="form-check-input mt-0"
                  {...register("emotion")}
                  type="checkbox"
                  defaultValue="Happy"
                  aria-label="Checkbox for following text input"
                />
              </div>
              <input
                type="text"
                defaultValue="Happy"
                className="form-control"
                disabled
                aria-label="Text input with checkbox"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="row mb-2">
        <div className="col">
          <label htmlFor="points">Points (between 0 and 10):</label>
          <input
            type="range"
            id="points"
            {...register("points")}
            min="0"
            max="10"
          ></input>
        </div>
      </div>
      {/* button Submit */}
      <div className="row mb-2">
        <div className="col">
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </div>
      </div>
    </form>
  );
};
